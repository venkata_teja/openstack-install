#!/bin/bash
#
# Initial data for Keystone using python-keystoneclient
#
# Tenant               User      Roles
# ------------------------------------------------------------------
# admin                admin     admin
# service              glance    admin
# service              nova      admin, [ResellerAdmin (swift only)]
# service              quantum   admin        # if enabled
# service              swift     admin        # if enabled
# demo                 admin     admin
# demo                 demo      Member, anotherrole
# invisible_to_admin   demo      Member
#

function get_id () {
    echo `$@ | awk '/ id / { print $4 }'`
}

# Tenants
TENANT_ADMIN=$(get_id keystone tenant-create --name=$TENANT_ADMIN_NAME)
TENANT_SERVICE=$(get_id keystone tenant-create --name=$TENANT_SERVICE_NAME)
TENANT_DEMO=$(get_id keystone tenant-create --name=$TENANT_DEMO_NAME)
TENANT_INVIS=$(get_id keystone tenant-create --name=invisible_to_admin)


# Users
USER_ADMIN=$(get_id keystone user-create --name="$KEYSTONE_ADMIN_USER_NAME"\
                                         --pass="$KEYSTONE_ADMIN_USER_PASSWORD" \
                                         --email="$KEYSTONE_ADMIN_USER_EMAIL")
USER_DEMO=$(get_id keystone user-create --name="$KEYSTONE_DEMO_USER_NAME"\
                                        --pass="$KEYSTONE_DEMO_USER_PASSWORD" \
                                        --email="$KEYSTONE_DEMO_USER_EMAIL")


# Roles
ROLE_ADMIN=$(get_id keystone role-create --name="$KEYSTONE_ADMIN_ROLE")
ROLE_MEMBER=$(get_id keystone role-create --name="$KEYSTONE_ROLE_MEMBER")
ROLE_KEYSTONEADMIN=$(get_id keystone role-create --name="$KEYSTONE_KEYSTONEADMIN_ROLE")
ROLE_KEYSTONESERVICE=$(get_id keystone role-create --name="$KEYSTONE_KEYSTONESERVICEADMIN_ROLE")
# ANOTHER_ROLE demonstrates that an arbitrary role may be created and used
# TODO(sleepsonthefloor): show how this can be used for rbac in the future!
ROLE_ANOTHER=$(get_id keystone role-create --name=anotherrole)


# Add Roles to Users in Tenants
keystone user-role-add --user $USER_ADMIN --role $ROLE_ADMIN --tenant_id $TENANT_ADMIN
keystone user-role-add --user $USER_ADMIN --role $ROLE_ADMIN --tenant_id $TENANT_DEMO
keystone user-role-add --user $USER_DEMO --role $ROLE_ANOTHER --tenant_id $TENANT_DEMO

# TODO(termie): these two might be dubious
keystone user-role-add --user $USER_ADMIN --role $ROLE_KEYSTONEADMIN --tenant_id $TENANT_ADMIN
keystone user-role-add --user $USER_ADMIN --role $ROLE_KEYSTONEADMIN --tenant_id $TENANT_ADMIN


# The Member role is used by Horizon and Swift so we need to keep it:
ROLE_MEMBER=$(get_id keystone role-create --name=Member)
keystone user-role-add --user $USER_DEMO --role $ROLE_MEMBER --tenant_id $TENANT_DEMO
keystone user-role-add --user $USER_DEMO --role $ROLE_MEMBER --tenant_id $TENANT_INVIS


# Configure service users/roles
USER_NOVA=$(get_id keystone user-create --name="$KEYSTONE_NOVA_USER_NAME"\
                                        --pass="$KEYSTONE_NOVA_USER_PASSWORD" \
                                        --tenant_id "$TENANT_SERVICE" \
                                        --email="$KEYSTONE_NOVA_USER_EMAIL")
keystone user-role-add --tenant_id $TENANT_SERVICE \
                       --user $USER_NOVA \
                       --role $ROLE_ADMIN

USER_GLANCE=$(get_id keystone user-create --name="$KEYSTONE_GLANCE_USER_NAME"\
                                          --pass="$KEYSTONE_GLANCE_USER_PASSWORD" \
                                          --tenant_id "$TENANT_SERVICE" \
                                          --email="$KEYSTONE_GLANCE_USER_EMAIL")
keystone user-role-add --tenant_id $TENANT_SERVICE \
                       --user $USER_GLANCE \
                       --role $ROLE_ADMIN

if [[ "$ENABLED_SERVICES" =~ "swift" ]]; then
    USER_SWIFT=$(get_id keystone user-create --name="$KEYSTONE_SWIFT_USER_NAME"\
                                             --pass="$KEYSTONE_SWIFT_USER_PASSWORD" \
                                             --tenant_id "$TENANT_SERVICE" \
                                             --email="$KEYSTONE_SWIFT_USER_EMAIL")
    keystone user-role-add --tenant_id $TENANT_SERVICE \
                           --user $USER_SWIFT \
                           --role $ROLE_ADMIN
    # Nova needs ResellerAdmin role to download images when accessing
    # swift through the s3 api. The admin role in swift allows a user
    # to act as an admin for their tenant, but ResellerAdmin is needed
    # for a user to act as any tenant. The name of this role is also
    # configurable in swift-proxy.conf
    ROLE_RESELLER=$(get_id keystone role-create --name=ResellerAdmin)
    keystone user-role-add --tenant_id $TENANT_SERVICE \
                           --user $USER_NOVA \
                           --role $ROLE_RESELLER
fi

if [[ "$ENABLED_SERVICES" =~ "quantum" ]]; then
    USER_QUANTUM=$(get_id keystone user-create --name="$KEYSTONE_QUANTUM_USER_NAME"\
                                               --pass="$KEYSTONE_QUANTUM_USER_PASSWORD" \
                                               --tenant_id "$TENANT_SERVICE" \
                                               --email="$KEYSTONE_QUANTUM_USER_EMAIL")
    keystone user-role-add --tenant_id $TENANT_SERVICE \
                           --user $USER_QUANTUM \
                           --role $ROLE_ADMIN
fi
